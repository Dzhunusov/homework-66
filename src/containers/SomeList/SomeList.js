import React, {useEffect, useState} from 'react';
import './SomeList.css';
import axiosOrders from "../../axios-orders";
import ListItem from "../../components/ListItem/ListItem";

const SomeList = () => {
  const [lists, setLists] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const listResponse = await axiosOrders.get('/message.json');
      const fetchedLists = Object.keys(listResponse.data).map(id => {
        return {...listResponse.data[id],id}
      });
      setLists(fetchedLists);
    }
    fetchData().catch(console.error);
  },[]);


  let listsResult = lists.map(list => (
      <ListItem
          key={list.id}
          customer={list.customer.name}
          message={list.customer.textarea}
      />
  ));

  return (
      <div className="main-block">
        {listsResult}
      </div>
  );
};

export default SomeList;