import React from 'react';
import './ListItem.css';

const ListItem = props => {
  return (
      <div className="list-item">
        <p className="author"><strong>Author</strong> : {props.customer}</p>
        <p><strong>Message</strong> : {props.message}</p>
      </div>
  );
};

export default ListItem;