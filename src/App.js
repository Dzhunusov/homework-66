import React from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import SomeList from "./containers/SomeList/SomeList";

function App() {
  return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={SomeList}/>
        </Switch>
      </BrowserRouter>
  );
}

export default App;
